import os
import re
import subprocess



def convert_fmt_to_re(name):
    pattern = re.compile(r'\%\d{1,2}d')
    fmt = pattern.findall(name)
    if len(fmt) != 1:
        raise Exception("Wildcards used more than once")
    f = fmt[0]
    d = f.strip('%d')
    mx = re.findall(r'[1-9]', d)[0]
    pad = '0' in d
    if pad:
        sub = r'\\d{' + mx + '}'
    else:
        sub = r'\\d{1,' + mx + '}'
    new_name = pattern.sub(sub, name)

    return new_name


def count_images(img_dir):
    dir = os.path.dirname(img_dir)  # directory
    bname = os.path.basename(img_dir)  # basename
    new_name = convert_fmt_to_re(bname)  # basename but with regex pattern
    images = [os.path.join(dir, y) for y in [x for x in os.walk(dir)][0][2] if re.findall(new_name, y, flags=re.IGNORECASE)]

    return len(images)


def images_to_video(image_files, output_file, fps, qual, ffmpeg_bin, target_dur):
    t = count_images(image_files) / target_dur  # input framerate (not output fps)
    if qual == 0:  # lossless
        opts = ["-vcodec", "ffv1"]
    else:
        bitrate = 10000 if qual == 1 else 2000
        opts = ["-vcodec", "mpeg4", "-b:v", str(bitrate) + "K"]

    cmd = [ffmpeg_bin, "-f", "image2", '-framerate', f'{t}']
    cmd.extend(['-t', f'{target_dur}', '-i', image_files])
    cmd.extend(opts)
    cmd.extend(['-r', f'{fps}', '-y', output_file])

    res = subprocess.call(cmd, stdin=subprocess.PIPE)

    return res == 0


def add_whitespace(ax, fx, fy):
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()

    # whitespace on x axis
    td0 = ax.transData.transform((xmin, ymin))
    td1 = ax.transData.transform((xmax, ymax))
    td2 = td0[0] - (td1[0] - td0[0]) * fx
    td3 = td1[0] + (td1[0] - td0[0]) * fx
    xmin = ax.transData.inverted().transform((td2, td0[1]))[0]
    xmax = ax.transData.inverted().transform((td3, td1[1]))[0]
    ax.set_xlim([xmin, xmax])

    # whitespace on y axis
    td0 = ax.transData.transform((xmin, ymin))
    td1 = ax.transData.transform((xmax, ymax))
    td2 = td0[1] - (td1[1] - td0[1]) * fy
    td3 = td1[1] + (td1[1] - td0[1]) * fy
    ymin = ax.transData.inverted().transform((td0[0], td2))[1]
    ymax = ax.transData.inverted().transform((td1[0], td3))[1]
    ax.set_ylim([ymin, ymax])