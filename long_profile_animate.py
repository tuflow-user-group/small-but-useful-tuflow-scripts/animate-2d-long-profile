import sys
import tempfile
import shutil
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from long_profile_data_provider import LongProfileDataProvider
from helpers import add_whitespace, images_to_video


__VERSION__ = '0.1'
FFMPEG = r"C:\Users\esymons\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\tuflow\ffmpeg-4.4-essentials_build\bin\ffmpeg.exe"


def main():
    # collect arguments
    inputs = []
    include_bed = False
    include_min = False
    include_max = False
    output_name = None
    output_duration = 15
    args = sys.argv[1:]
    while args:
        if args[0].lower() == '-v':
            print(f'Version: {__VERSION__}')
        elif args[0].lower() == '-max':
            include_max = True
        elif args[0].lower() == '-min':
            include_min = True
        elif args[0].lower() == '-bed':
            include_bed = True
        elif args[0].lower() == '-i':
            if len(args) < 2:
                sys.exit('No argument provided after -i')
            args = args[1:]
            inputs.append(args[0])
        elif args[0].lower() == '-o':
            if len(args) < 2:
                sys.exit('No argument provided after -o')
            args = args[1:]
            output_name = args[0]
        elif args[0].lower() == '-dur':
            if len(args) < 2:
                sys.exit('No argument provided after -dur')
            args = args[1:]
            output_duration = float(args[0])
        else:
            sys.exit(f'Unrecognised argument: {args[0]}')

        args = args[1:]

    # check there are inputs
    if not inputs:
        sys.exit('No inputs found')

    # create output name
    if not output_name:
        output_name = 'long_profile_output.mp4'
    elif Path(output_name).suffix == '':
        output_name = f'{output_name}.mp4'
    output_file = Path('.') / output_name

    # check for ffmpeg.exe - either specified in FFMPEG or it can also be sitting in working directory
    if Path(FFMPEG).exists():
        ffmpeg = FFMPEG
    elif (Path('.') / 'ffmpeg.exe').exists():
        ffmpeg = str(Path('.') / 'ffmpeg.exe')
    else:
        sys.exit('Can not find ffmpeg.exe')

    # load long profiles
    lps = [LongProfileDataProvider(x) for x in inputs]

    # work out plot extents
    xmin, xmax = 9e29, -9e29
    ymin, ymax = 9e29, -9e29
    timesteps = []  # also collect all unique times from all results
    for lp in lps:
        i = 1 if include_bed else 2
        xmin = min(xmin, np.nanmin(lp[:,0]))
        xmax = max(xmax, np.nanmax(lp[:,0]))
        ymin = min(ymin, np.nanmin(lp[:,i:]))
        ymax = max(ymax, np.nanmax(lp[:,i:]))

        if include_max:
            ymax = max(ymax, np.nanmax(lp.max[:,1]))
        if include_min:
            ymin = min(ymin, np.nanmin(lp.min[:,1]))

        timesteps.extend([x for x in lp.time if x not in timesteps])

    tmpdir = tempfile.mkdtemp(prefix='longplot_animation')  # temp directory to save figures to - will be deleted at end

    try:
        for i, time in enumerate(timesteps):
            # logging stuff - so we know what timestep we're up to
            print(f'{time:.2f} ', end='', flush=True)
            if i > 0 and i % 20 == 0 or i + 1 == len(timesteps):
                print()

            # initialise plot
            fig, ax = plt.subplots()
            fig.set_size_inches(8.25, 5.875)  # A5 landscape

            for lp in lps:
                if time in lp.time:
                    j = lp.time.index(time) + 2
                else:
                    continue

                # plot
                if include_bed:
                    ax.plot(lp[:,0], lp[:,1], label='Bed Elevation', color='black')
                if include_min:
                    ax.plot(lp.min[:,0], lp.min[:,2], label=f'{lp.label}_min', linestyle=':')
                if include_max:
                    ax.plot(lp.max[:,0], lp.max[:,2], label=f'{lp.label}_max', linestyle='--')
                ax.plot(lp[:,0], lp[:,j], label=f'{lp.label}')

            # limits - set this so each plot is the same
            ax.set_xlim((xmin, xmax))
            ax.set_ylim((ymin, ymax))
            add_whitespace(ax, 0.05, 0.1)  # since we're manually setting limits, also manually add some whitespace

            # grid lines
            ax.grid(which='major', axis='both', linestyle='--')

            # legend
            fig.legend(loc='upper right')

            # title
            fig.suptitle(f'Time: {time:.02f}', x=0.1)

            # save figure
            outfile = Path(tmpdir) / f'longplot_animation_{i:03d}.png'
            fig.savefig(str(outfile))

            plt.close(fig)

        # create animation
        images = Path(tmpdir) / f'longplot_animation_%03d.png'
        images_to_video(str(images), output_file, 30, 1, ffmpeg, output_duration)
    except Exception as e:
        print(f'\nSomething went wrong exporting figures/animation: {e}')
    finally:
        shutil.rmtree(tmpdir)


if __name__ == '__main__':
    main()
